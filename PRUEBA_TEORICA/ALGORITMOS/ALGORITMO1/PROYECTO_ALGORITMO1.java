package org.example;

import java.util.Random;

public class PROYECTO_ALGORITMO1 {
    public static void main(String[] args) {
        crearGruposPlaca("Guayas","Vehículo particular",2);
        System.out.println("----------------------------------------");
        crearGruposPlaca("Azuay","Vehículos de uso oficial",3);
    }
//Metodo para genrear una placa
    public static String generarPlacas(String provincia, String tipo) {
        Random random = new Random();
        String primeraLetra;
        String segundaLetra;
        int numero = random.nextInt(7) + 3; // Número entre 3 y 9

        // Vectores de provincias y tipos con sus respectivas letras
        String[] provincias = {"Guayas", "Pichincha", "Manabi", "Azuay", "Santa Elena", "Loja", "El Oro", "Napo", "Orellana", "Carchi"};
        String[] letrasProvincia = {"G", "P", "M", "A", "Y", "L", "O", "N", "Q", "C"};
        String[] tipos = {"Vehículos comerciales", "Vehículos gubernamentales", "Vehículos de uso oficial", "Vehículo particular"};
        String[] letrasTipo = {"AUZ", "E", "X", "BCDFGHIJKLMNOPQRSTVWY"};

        // Buscar la primera letra basada en la provincia
        int indexProvincia = -1;
        for (int i = 0; i < provincias.length; i++) {
            if (provincia.equals(provincias[i])) {
                indexProvincia = i;
                break;
            }
        }
        primeraLetra = (indexProvincia != -1) ? letrasProvincia[indexProvincia] : "La provincia no está en la lista";

        // Buscar la segunda letra basada en el tipo
        int indexTipo = -1;
        for (int i = 0; i < tipos.length; i++) {
            if (tipo.equals(tipos[i])) {
                indexTipo = i;
                break;
            }
        }
        segundaLetra = (indexTipo != -1) ? String.valueOf(letrasTipo[indexTipo].charAt(random.nextInt(letrasTipo[indexTipo].length()))) : "El tipo no está en la lista";

        // Unir la placa
        return primeraLetra + segundaLetra + numero + "-" +
                random.nextInt(10) + "" + random.nextInt(10) + "" +
                random.nextInt(10) + "" + random.nextInt(10);
    }


    //Método para generar el grupo de placas
    public static void crearGruposPlaca(String provincia, String tipo, Integer numeroBloques){
        int maxNum = Integer.MAX_VALUE;
        //Control para definir que un número sea positivo y no mayor al entero máximo permitido
        if (numeroBloques < 1 && numeroBloques > maxNum) {
            System.out.println("Ingrese un número válido");
        } else {
            for (int i = 0; i < numeroBloques; i++) {
                for (int j = 0; j < 3; j++) {
                    String placa = generarPlacas(provincia, tipo);
                    System.out.println(placa);
                }
                System.out.println(" ");
            }
        }
    }


}

