package org.example;

public class PROYECTO_ALGORITMO2 {
    public static void main(String[] args) {

        System.out.println(tratarCadena("playaSalinas"));
        System.out.println(tratarCadena("casarota"));

    }
    public static String tratarCadena(String cadena)
    {
        if (cadena.length() % 2 == 0) {
            int mitad = cadena.length() / 2;
            String parte1 = cadena.substring(0, mitad);
            String parte2 = cadena.substring(mitad);
            String resultado = parte2 + parte1;
            System.out.println("Cadena: " + cadena);
            System.out.println("Resultado: " + resultado);
        } else {
            // Si la longitud de la cadena es impar
            System.out.println("La cadena es impar y tiene " + cadena.length() + " caracteres.");
        }
        return "";
    }
}

