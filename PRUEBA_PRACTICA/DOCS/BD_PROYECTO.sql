
-- Crear la base de datos "proyecto"
CREATE DATABASE IF NOT EXISTS proyecto;

-- Seleccionar la base de datos "proyecto"
USE proyecto;

-- Crear la tabla "Producto"
CREATE TABLE IF NOT EXISTS producto (
    id_producto INT AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(100),
    descripcion VARCHAR(500),
    valor DECIMAL(10, 2),
    marca VARCHAR(100),
    fecha_creacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    fecha_venta TIMESTAMP,
    imagen VARCHAR(500),
    estado VARCHAR(50) DEFAULT 'Activo'
);
-- Crear la tabla "Usuarios"
CREATE TABLE usuarios (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(50) NOT NULL UNIQUE,
    email VARCHAR(100),
    password VARCHAR(100) NOT NULL
);
