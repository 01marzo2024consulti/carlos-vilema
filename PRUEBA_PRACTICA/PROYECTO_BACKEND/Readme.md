# Evaluación Práctica 

Este es el backend de la evaluación práctica para el sistema de gestión de productos.

## Requisitos

- Springboot 3.2.3
- Java 17
- MySQL
- Maven
- Gestor de base de datos (MySQL)
- Cliente de Git

## Configuración del proyecto

1. Clona este repositorio en tu máquina local usando Git: git clone https://gitlab.com/01marzo2024consulti/carlos-vilema.git
2. Abre el proyecto en tu IDE preferido (Este proyecto se desarrolló en Intellij).
3. Configura las credenciales de la base de datos en `application.properties`.
4. Asegúrate de tener todas las dependencias necesarias en el archivo `pom.xml`.

### Base de Datos

1. Importa el archivo `BD_PROYECTO.sql` de la carpeta DOCS del repositorio con el nombre en tu base de datos MySQL para crear la estructura inicial. Este script creará las tablas necesarias.

## Ejecución

1. Ejecuta la aplicación desde tu IDE al archivo proyBack.java

## Uso

1. Accede mediante la URL base: htpp://localhost:8082/"api".
2. Probar mediante postman o cualquier herramienta de tu preferencia

##Levantar el Docker
1. Construir la imagen con docker-compose build
2. Correr el proyecto con docker-compose up