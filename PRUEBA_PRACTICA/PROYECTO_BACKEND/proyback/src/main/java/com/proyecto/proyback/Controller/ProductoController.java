package com.proyecto.proyback.Controller;

import com.proyecto.proyback.Entity.Producto;
import com.proyecto.proyback.Service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200") // Permite solicitudes CORS solo desde http://localhost:4200
@RequestMapping("/producto")
public class ProductoController {
    @Autowired
    private ProductoService productoService;

    //Listar todos
    @GetMapping("/listar")
    public ResponseEntity<List<Producto>> listar() {
        List<Producto> productos = productoService.listar();
        return new ResponseEntity<>(productos, HttpStatus.OK);
    }

    //Listar uno
    @GetMapping("listaruno/{id}")
    public ResponseEntity<Producto> listarUno(@PathVariable Long id) {
        Producto producto = productoService.listarUno(id);
        if (producto != null) {
            return new ResponseEntity<>(producto, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //Guardar
    @PostMapping("/guardar")
    public ResponseEntity<Producto> crear(@RequestBody Producto producto) {
        Producto nuevoProducto = productoService.guardar(producto);
        return new ResponseEntity<>(nuevoProducto, HttpStatus.CREATED);
    }

    // Eliminar
    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<Producto> eliminar(@PathVariable Long id) {
        Producto productoEliminado = productoService.eliminar(id);
        if (productoEliminado != null) {
            return new ResponseEntity<>(productoEliminado, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/updateImagen/{idProducto}")
    public ResponseEntity<Producto> updateImagen(@PathVariable Long id, @RequestBody String imgB64) {
        try {
            return ResponseEntity.ok(productoService.updateImagen(imgB64, id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }

    }


}
