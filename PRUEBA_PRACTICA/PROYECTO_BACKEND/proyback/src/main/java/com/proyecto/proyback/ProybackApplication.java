package com.proyecto.proyback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProybackApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProybackApplication.class, args);
	}

}
