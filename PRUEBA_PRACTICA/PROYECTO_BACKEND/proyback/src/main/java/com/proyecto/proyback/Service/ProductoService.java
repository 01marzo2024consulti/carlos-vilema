package com.proyecto.proyback.Service;

import com.proyecto.proyback.Entity.Producto;
import com.proyecto.proyback.Repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProductoService {
    @Autowired
    private final ProductoRepository productoRepository;

    public ProductoService(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    //Método par listar todos los elementos
    public List<Producto> listar() {
        return productoRepository.findAll();
    }

    //Método par listar un elemento
    public Producto listarUno(Long id) {
        Optional<Producto> optionalProducto = productoRepository.findById(id);
        if (optionalProducto.isPresent()) {
            Producto producto = optionalProducto.get();
            if ("Activo".equals(producto.getEstado())) {
                return producto;
            }
        }
        return null;
    }

    //Método para guardar un elemento
    public Producto guardar (Producto p) {
        p.setFechaCreacion(new Date()); // fecha/hora actual del servidor
        p.setEstado("Activo");// Establecer como "Activo" por defecto
        if (p.getNombre() == null || p.getNombre().isEmpty()) {p.setNombre("N/A");}
        if (p.getDescripcion() == null || p.getDescripcion().isEmpty()) {p.setDescripcion("N/A");}
        if (p.getMarca() == null || p.getMarca().isEmpty()) {p.setMarca("N/A");}
        if (p.getImagen() == null || p.getImagen().isEmpty()) {p.setImagen("N/A");}
        return productoRepository.save(p);
    }


    //Metodos para el borrado lógico de un elemento
    public Producto eliminar(Long id) {
        Producto producto = productoRepository.findById(id).orElse(null);
        if (producto != null) {
            producto.setEstado("Inactivo");
            return productoRepository.save(producto);
        } else {
            return null;
        }
    }
    //Metodo para guardar una imagen
    public Producto updateImagen(String imgB64, Long id) throws Exception {
        Producto producto = productoRepository.findById(id).orElse(null);
        if (producto == null) {throw new Exception("No se registra el producto");}
        producto.setImagen(imgB64);
        return productoRepository.save(producto);

    }


}
