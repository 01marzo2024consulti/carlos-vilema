package com.proyecto.proyback.Entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "producto")
public class Producto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_producto")
    private Long idProducto;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "valor")
    private double valor;

    @Column(name = "marca")
    private String marca;

    @Column(name = "fecha_creacion")
    private Date fechaCreacion;

    @Column(name = "fecha_venta")
    private Date fechaVenta;

    @Column(name = "imagen")
    private String imagen;

    @Column(name = "estado")
    private String estado;
}
