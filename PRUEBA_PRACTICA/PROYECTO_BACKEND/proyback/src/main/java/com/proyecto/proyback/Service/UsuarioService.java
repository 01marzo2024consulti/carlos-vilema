package com.proyecto.proyback.Service;

import com.proyecto.proyback.Entity.Usuario;
import com.proyecto.proyback.Repository.ProductoRepository;
import com.proyecto.proyback.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {
    @Autowired
    private final UsuarioRepository usuarioRepository;

    public UsuarioService(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }


    public boolean autenticar(String username, String password) {
        Usuario usuario = usuarioRepository.findByUsername(username);
        if (usuario != null) {
            return usuario.getPassword().equals(password);
        }
        return false;
    }
}
