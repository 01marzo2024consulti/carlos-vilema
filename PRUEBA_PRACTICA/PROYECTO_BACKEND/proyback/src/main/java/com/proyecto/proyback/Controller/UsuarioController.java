package com.proyecto.proyback.Controller;

import com.proyecto.proyback.Entity.Usuario;
import com.proyecto.proyback.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/usuario")
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;

    @PostMapping("/login")
    public boolean login(@RequestBody Usuario usuario) {
        return usuarioService.autenticar(usuario.getUsername(), usuario.getPassword());
    }
}
