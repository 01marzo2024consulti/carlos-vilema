# Evaluación Práctica

Este es el proyecto frontend de la evaluación práctica desarrollado en Angular.

## Requisitos Previos

Antes de comenzar, asegúrate de tener instalado:

- Node.js y npm: [Descargar aquí](https://nodejs.org/)
- Angular CLI: Ejecuta `npm install -g @angular/cli` para instalarlo globalmente

## Instalación

1. Clona este repositorio:

   ```bash
   git clone https://gitlab.com/01marzo2024consulti/carlos-vilema.git
   ```

2. Navega al directorio del proyecto:

   ```bash
   cd proyfront
   ```

3. Instala las dependencias:

   ```bash
   npm install
   ```

## Uso

Para ejecutar el proyecto en modo de desarrollo, utiliza el siguiente comando:

```bash
ng serve
```

Visita `http://localhost:4200/"ruta_componente"` en tu navegador para ver la aplicación.

## Docker
1. Construir la imagen con docker build.
2. Correr el docker con el comando docker run -p 4200:80 'imagen_id'

## Contribuciones

¡Las contribuciones son bienvenidas!

### Nota
El backend usa spring security por lo que si requiere cambiar las credenciales, debe hacerlo en el archivo productoservice.ts
Los archivos Docker se encuentran configurados con las credenciales del proyecto, si desea hacer algun cambio puede hacerlo en el archivo .env que se encuentra en la raiz del proyecto