import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AgregarProdComponent } from './components/agregar-prod/agregar-prod.component';
import { ListarProdComponent } from './components/listar-prod/listar-prod.component';
import { ListarunProdComponent } from './components/listarun-prod/listarun-prod.component';
import { FiltroPipe } from './pipes/filtro.pipe';
import { environment } from "../environments/environment";
import { initializeApp } from "firebase/app";
initializeApp(environment.firebase);

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AgregarProdComponent,
    ListarProdComponent,
    ListarunProdComponent,
    FiltroPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [FiltroPipe],
  providers: [
    provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
