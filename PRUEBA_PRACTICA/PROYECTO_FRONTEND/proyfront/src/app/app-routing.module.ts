import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { AgregarProdComponent } from './components/agregar-prod/agregar-prod.component';
import { ListarProdComponent } from './components/listar-prod/listar-prod.component';
import { ListarunProdComponent } from './components/listarun-prod/listarun-prod.component';

const routes: Routes = [
  { path: "login", component: LoginComponent},
  { path: "agregar-prod", component: AgregarProdComponent},
  { path: "listar-prod", component: ListarProdComponent},
  { path: "listarun-prod", component: ListarunProdComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
