import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarunProdComponent } from './listarun-prod.component';

describe('ListarunProdComponent', () => {
  let component: ListarunProdComponent;
  let fixture: ComponentFixture<ListarunProdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListarunProdComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ListarunProdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
