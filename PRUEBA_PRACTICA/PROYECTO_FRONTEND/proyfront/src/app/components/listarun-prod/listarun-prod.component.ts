import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductoService } from '../../services/producto.service';

@Component({
  selector: 'app-listarun-prod',
  templateUrl: './listarun-prod.component.html',
  styleUrl: './listarun-prod.component.css'
})

  export class ListarunProdComponent {
    producto: any;

    constructor(private route: ActivatedRoute, private productoService: ProductoService) { }
  
    ngOnInit(): void {
      this.route.paramMap.subscribe(params => {
        const productId = Number(params.get('id'));
      this.obtenerDetalleProducto();
      });
    }
  
    obtenerDetalleProducto() {
      const idProducto = this.route.snapshot.params['id']; // Obtener el ID del producto de la URL
      this.productoService.obtenerProductoPorId(idProducto).subscribe(
        (response: any) => {
          this.producto = response;
        },
        (error) => {
          console.error('Error al obtener el detalle del producto:', error);
        }
      );
    }
}
