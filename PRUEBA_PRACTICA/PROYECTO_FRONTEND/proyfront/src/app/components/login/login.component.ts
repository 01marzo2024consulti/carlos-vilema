import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  usuario: string="";
  contrasena: string="";

  constructor(private router: Router) { }

  login(formulario: NgForm) {
    if (formulario.invalid) {
      return;
    }
    //lógica para iniciar sesión
    console.log('Usuario:', this.usuario);
    console.log('Contraseña:', this.contrasena);
    //servicio para autenticar al usuario
  }
  navigateToListarProd() {
    this.router.navigate(['/listar-prod']);
  }
}
