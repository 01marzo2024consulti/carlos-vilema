import { Component } from '@angular/core';
import { ProductoService } from '../../services/producto.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-listar-prod',
  templateUrl: './listar-prod.component.html',
  styleUrl: './listar-prod.component.css'
})
export class ListarProdComponent {

  productos: any[] = [];
  productosFiltrados: any[] = [];
  filtroNombre: string = '';
  filtroFechaIngreso: string = '';
  filtroFechaVenta: string = '';
  filtroValor: number  = 0;
  filtroEstado: string = '';

  constructor(private productoService: ProductoService, private router: Router) { }

  ngOnInit(): void {
    this.cargarProductos();
  }

  cargarProductos() {
    this.productoService.obtenerProductos().subscribe(
      (response: any) => {
        this.productos = response;
        this.aplicarFiltros();
      },
      (error) => {
        console.error('Error al cargar productos:', error);
      }
    );
  }

  verDetalles(producto: any) {
    this.router.navigate(['/listarun-prod']);
  }
  /*eliminarProducto1(producto: any) {
    console.log('Eliminar producto:', producto);
  }*/
  eliminarProducto(producto: any): void {
    this.productoService.eliminarProductoPorId(producto.id).subscribe(
      () => {
        console.log('Producto eliminado correctamente.');
        this.cargarProductos(); 
      },
      (error) => {
        console.error('Error al eliminar el producto:', error);
      }
    );
  }
  agregarProducto() {
    this.router.navigate(['/agregar-prod']);
  }

  aplicarFiltros() {
    this.productosFiltrados = this.productos.filter(producto => {
      // Filtra los productos según los criterios de búsqueda
      return (
        producto.nombre.toLowerCase().includes(this.filtroNombre.toLowerCase()) &&
        producto.fecha_creacion.includes(this.filtroFechaIngreso) &&
        producto.fecha_venta.includes(this.filtroFechaVenta) &&
        (this.filtroValor === null || producto.valor === this.filtroValor) &&
        producto.estado.toLowerCase().includes(this.filtroEstado.toLowerCase())
      );
    });
  }

  limpiarFiltros() {
    // Limpia todos los filtros y vuelve a cargar la lista de productos original
    this.filtroNombre = '';
    this.filtroFechaIngreso = '';
    this.filtroFechaVenta = '';
    this.filtroValor = 0;
    this.filtroEstado = '';
    this.productosFiltrados = [...this.productos];
  }

}
