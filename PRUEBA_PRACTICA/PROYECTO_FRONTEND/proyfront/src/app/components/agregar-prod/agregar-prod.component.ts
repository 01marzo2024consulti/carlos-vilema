import { Component } from '@angular/core';
import { ProductoService } from '../../services/producto.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-agregar-prod',
  templateUrl: './agregar-prod.component.html',
  styleUrl: './agregar-prod.component.css'
})
export class AgregarProdComponent {
  imagenBase64: string | ArrayBuffer | null = null;
  constructor(private productoService: ProductoService) { }
 producto :any = { };

  guardarProducto(productoForm: NgForm) {
    console.log(productoForm.value);
    // Verifica que se haya seleccionado una imagen
    /*if (!this.imagenBase64) {
      console.error('No se ha seleccionado ninguna imagen.');
      return;
    }*/
   
    
      // Agrega la imagen base64 al objeto del nuevo producto
      this.producto.imagen = this.imagenBase64;

      // Llama al método del servicio para guardar el producto
      this.productoService.guardarProducto(this.producto).subscribe(
        (response) => {
          console.log('Producto guardado exitosamente:', response);
          this.producto = {}; 
        },
        (error) => {
          console.error('Error al guardar el producto:', error);
        }
      ); 


}
  limpiarFormulario() {
    this.producto = {}; // Reiniciar el objeto del nuevo producto a un objeto vacío
  }
  
  onFileSelected(event: any) {
    
    const file: File = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imagenBase64 = reader.result;
      };
    }
  }

  onFileSelected2(event: any) {
    const file: File = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.imagenBase64 = reader.result;
    };
  }
}