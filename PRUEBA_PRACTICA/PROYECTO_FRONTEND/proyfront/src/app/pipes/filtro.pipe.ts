import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(productos: any[], filtroNombre: string, filtroFechaIngreso: string, filtroFechaVenta: string, filtroValor: number, filtroEstado: string): any[] {
    if (!filtroNombre && !filtroFechaIngreso && !filtroFechaVenta && !filtroValor && !filtroEstado) {
      return productos;
    }

    return productos.filter(producto => {
      if (filtroNombre && producto.nombre.toLowerCase().indexOf(filtroNombre.toLowerCase()) === -1) {
        return false;
      }
      if (filtroFechaIngreso && producto.fechaIngreso !== filtroFechaIngreso) {
        return false;
      }
      if (filtroFechaVenta && producto.fechaVenta !== filtroFechaVenta) {
        return false;
      }
      if (filtroValor && producto.valor !== filtroValor) {
        return false;
      }
      if (filtroEstado && producto.estado.toLowerCase() !== filtroEstado.toLowerCase()) {
        return false;
      }
      return true;
    });
  }

}
