import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private apiUrl = 'http://localhost:8082';
  private username = 'user';
  private password = '123';

  constructor(private http: HttpClient) { }

  guardarProducto(producto: any) {
    const headers = new HttpHeaders({
      'Authorization': 'Basic ' + btoa(this.username + ':' + this.password)
    });
    return this.http.post<any[]>(`${this.apiUrl}/producto/guardar`, producto,  { headers });
  }

  obtenerProductos(): Observable<any[]> {
    const headers = new HttpHeaders({
      'Authorization': 'Basic ' + btoa(this.username + ':' + this.password)
    });
    return this.http.get<any[]>(`${this.apiUrl}/producto/listar`, { headers });
  }
  obtenerProductoPorId(producto: any): Observable<any[]> {
    const headers = new HttpHeaders({
      'Authorization': 'Basic ' + btoa(this.username + ':' + this.password)
    });
    return this.http.get<any[]>(`${this.apiUrl}/producto/listaruno/1`, { headers });   
  }
  eliminarProductoPorId(id: number): Observable<void> {
    const headers = new HttpHeaders({
      'Authorization': 'Basic ' + btoa(this.username + ':' + this.password)
    });
    return this.http.delete<void>(`${this.apiUrl}/producto/eliminar/3`, { headers });//${id}
  }
  loguear(producto: any): Observable<void> {
    const headers = new HttpHeaders({
      'Authorization': 'Basic ' + btoa(this.username + ':' + this.password)
    });
    return this.http.delete<void>(`${this.apiUrl}/producto/login`, { headers });
  }


  /*enviarImagenBase64(imagenBase64: string) {
    const headers = new HttpHeaders({
      'Authorization': 'Basic ' + btoa(this.username + ':' + this.password)
    });
    return this.http.post<any[]>(`${this.apiUrl}/producto/guardar`, { headers });
  }*/

}
